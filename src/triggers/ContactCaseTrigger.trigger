Trigger ContactCaseTrigger on Contact (before delete, after insert) {
    if (Trigger.isInsert) {
        List<Case> cases = new List<Case>();
        for (Contact element : Trigger.new) {
            Case nCase = new Case();
            nCase.ContactId = element.Id;
            nCase.AccountId = element.AccountId;
            nCase.Status = 'Working';
            nCase.Origin = 'New Contact';
            if (element.Contact_Level__c == 'Primary') {
                cases.add(new Case(Priority = 'High'));
            } else if (element.Contact_Level__c == 'Secondary') {
                cases.add(new Case(Priority = 'Medium'));
            } else if (element.Contact_Level__c == 'Tertiary') {
                cases.add(new Case(Priority = 'Low'));
            }
            if (nCase.AccountId != null) {
                nCase.AccountId = element.AccountId;
                List<Account> accounts = new List<Account>([SELECT Id, OwnerId FROM Account WHERE Id = :element.AccountId]);
                nCase.OwnerId = accounts[0].OwnerId;
            }
            cases.add(nCase);
        }
        insert cases;
    } else if (Trigger.isDelete) {
        Set<Id> ContactId = Trigger.oldMap.keySet();
        delete [SELECT Id FROM Case WHERE ContactId IN :ContactId];
    }

}