@IsTest
public with sharing class ContactManagerControllerTest {

    @TestSetup
    public static void testDate() {
        Account acc = new Account(
                Name = 'TestAcc Gfd'
        );
        insert acc;
        List<Contact> contactsList = new List<Contact>();
        ContactManagerController controller = new ContactManagerController();
        controller.newFirstName = 'Test';
        controller.newLastName = 'Test1';
        controller.newEmail = 'test@mail.ru';
        controller.newPhone = '375291876543';
        controller.newAccount = 'AccountTest';
        controller.newContactLevel = 'Tertiary';
        insert contactsList;
    }

    @IsTest
    static void saveContactTest() {
        ContactManagerController controller = new ContactManagerController();
        controller.newAccount = 'TestAccount';
        controller.newFirstName = 'TestFirstName1';
        controller.newLastName = 'TestLastName1';
        controller.newEmail = 'TestEmai1@gmail.com';
        controller.newPhone = '+375291234567';
        controller.newContactLevel = 'Primary';
        controller.saveContact();
    }

    @IsTest static void DeleteContactTest() {
        ContactManagerController controller = new ContactManagerController();
        controller.newAccount = 'TestAccount1';
        controller.newFirstName = 'TestFirstName3';
        controller.newLastName = 'TestLastName3';
        controller.newEmail = 'TestEmail3@gmail.com';
        controller.newPhone = '+375333456789';
        controller.newContactLevel = 'Secondary';
        Contact contact = new Contact(
                FirstName = controller.newFirstName,
                LastName = controller.newLastName,
                Email = controller.newEmail,
                Phone = controller.newPhone,
                Contact_Level__c = controller.newContactLevel);
        insert contact;
        controller.SelectedRowId = contact.Id;
        controller.DeleteContact();
        controller.DeleteContact();
    }

    @IsTest
    public static void getContactTest() {
        ContactManagerController controller = new ContactManagerController() ;
        controller.getContactList();
        controller.sortByName();
        controller.sortByEmail();
        controller.sortByAccountID();
        controller.sortByContactLevel();
        controller.sortByOwnerID();
        controller.sortByCreatedById();
        controller.sortByCreatedDate();
        controller.popupClose();
        controller.popupOpen();
        controller.FirstPage();
        controller.previousPage();
        controller.nextPage();
        controller.LastPageContacts();
        controller.getNextPageContacts();
        controller.getPreviousPageContacts();
        controller.getContactLevel();
    }
}