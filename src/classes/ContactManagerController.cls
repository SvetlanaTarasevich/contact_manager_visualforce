public class ContactManagerController {

    public static String searchKeyword { get; set; }
    public Boolean displayPopup { get; set; }
    public String newFirstName { get; set; }
    public String newLastName { get; set; }
    public String newEmail { get; set; }
    public String newPhone { get; set; }
    public String newAccount { get; set; }
    public String newContactLevel { get; set; }
    public List<Contact> listContacts { get; set; }
    public String SelectedRowId { get; set; }
    public String sortingColumn = 'Name';
    public String sortingOrder = ' ASC ';
    public String column = sortingColumn;
    private Integer totalRecords = 0;
    private Integer OffsetSize = 0;
    private Integer LimitSize = 10;

    public ContactManagerController() {
        totalRecords = [
                SELECT count()
                FROM Contact
        ];
    }

    public List<SelectOption> getContactLevel() {
        List<SelectOption> ContactLevel = new List<SelectOption>();
        ContactLevel.add(new SelectOption('Primary', 'Primary'));
        ContactLevel.add(new SelectOption('Secondary', 'Secondary'));
        ContactLevel.add(new SelectOption('Tertiary', 'Tertiary'));
        return ContactLevel;
    }

    public List<Contact> getContactList() {
        String nameSegment = '\'%' + searchKeyword + '%\'';
        if (searchKeyword != null) {
            listContacts = Database.query('SELECT '
                    + 'Name, '
                    + 'Email, '
                    + 'Contact_Level__c, '
                    + 'Account.Name, '
                    + 'Owner.Name, '
                    + 'CreatedBy.Name, '
                    + 'CreatedDate '
                    + 'FROM '
                    + 'Contact '
                    + 'WHERE '
                    + 'Name '
                    + 'LIKE ' + nameSegment
                    + 'ORDER BY ' + sortingColumn + sortingOrder
                    + ' LIMIT ' + LimitSize + ' OFFSET ' + OffsetSize);
            return listContacts;

        } else if (sortingColumn != null) {
            if (sortingColumn == column) {
                sortingOrder = (sortingOrder == ' ASC ') ? ' DESC ' : ' ASC ';
            } else {
                sortingOrder = ' ASC ';
                column = sortingColumn;
            }
            listContacts = Database.query('SELECT '
                    + 'Name, '
                    + 'Email, '
                    + 'Contact_Level__c, '
                    + 'Account.Name, '
                    + 'Owner.Name, '
                    + 'CreatedBy.Name, '
                    + 'CreatedDate '
                    + 'FROM '
                    + 'Contact '
                    + 'ORDER BY ' + sortingColumn + sortingOrder
                    + 'LIMIT ' + LimitSize + ' OFFSET ' + OffsetSize);
            return listContacts;

        }
        return listContacts;
    }

    public void sortByName() {
        sortingColumn = 'Name';
        getContactlist();
    }

    public void sortByEmail() {
        sortingColumn = 'Email ';
        getContactlist();
    }

    public void sortByContactLevel() {
        sortingColumn = 'Contact_Level__c';
        getContactlist();
    }

    public void sortByAccountID() {
        sortingColumn = 'Account.Name';
        getContactlist();
    }

    public void sortByOwnerID() {
        sortingColumn = 'Owner.Name';
        getContactlist();
    }

    public void sortByCreatedById() {
        sortingColumn = 'CreatedById';
        getContactlist();
    }

    public void sortByCreatedDate() {
        sortingColumn = 'CreatedDate';
        getContactlist();
    }

    public void popupOpen() {
        displayPopup = true;
    }

    public void popupClose() {
        displayPopup = false;
    }

    public void saveContact() {
        if (newAccount != '') {
            Account acc = new Account(Name = newAccount);
            insert acc;
            Contact con = new Contact(
                    FirstName = newFirstName,
                    LastName = newLastName,
                    Email = newEmail,
                    Phone = newPhone,
                    Contact_Level__c = newContactLevel,
                    AccountId = acc.ID
            );
            insert con;
            popupClose();
        }
    }

    public void DeleteContact() {
        String SelectedRowId = ApexPages.CurrentPage().getParameters().get('rowId');
        if (SelectedRowId == null) {
            return;
        }
        Contact contact = null;
        for (Contact c : [SELECT Id FROM Contact]) {
            if (c.Id == SelectedRowId) {
                contact = c;
                break;
            }
            if (contact != null) {
                delete contact;
            }
        }
    }

    public void FirstPage() {
        OffsetSize = 0;
    }

    public void PreviousPage() {
        OffsetSize = OffsetSize - LimitSize;
    }

    public void NextPage() {
        OffsetSize = OffsetSize + LimitSize;
    }

    public void LastPageContacts() {
        OffsetSize = totalRecords - Math.mod(totalRecords, LimitSize);
    }

    public Boolean getPreviousPageContacts() {
        if (OffsetSize == 0) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean getNextPageContacts() {
        if ((OffsetSize + LimitSize) > totalRecords) {
            return true;
        } else {
            return false;
        }
    }
}